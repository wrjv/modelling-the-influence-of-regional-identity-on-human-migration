#  Modelling the Influence of Regional Identity on Human Migration 

This repository contains code related to the publication by Willem R. J. Vermeulen, Debraj Roy and Rick Quax.

The repository contains Pipfiles that can be used with [Pipenv](https://github.com/pypa/pipenv) to set up a working environment with ease. Once set up, run `run.py`. It will generate all information needed, as well as graphs that are saved in the output folder.