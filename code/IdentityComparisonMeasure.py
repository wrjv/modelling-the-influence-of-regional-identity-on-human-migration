#!/usr/bin/env python3

'''
This file defines the Identity Comparison Measure, which can be used to
measure and compare the effects of identity in certain sets of identity
regions.

Written by Willem R.J.Vermeulen.
'''

import pickle
import csv

from random import randint

import numpy as np
import matplotlib.pyplot as plt

from matplotlib.font_manager import FontProperties
from sklearn.cluster import KMeans
from scipy.stats import ks_2samp

from .MigrationDataSource import MigrationDataSource
from .CitizenDataSource import CitizenDataSource
from .LocationDataSource import LocationDataSource


class IdentityComparisonMeasure(object):
    '''
    Basic class used to compare different definitions of identity
    areas. Can also create random regions.
    '''

    lds = LocationDataSource(["NL"])

    def __init__(self, years=[year for year in range(1996, 2017)],
                 file_location="data/"):
        '''

        '''

        # Save the variables.
        self.years = years
        self.file_location = file_location

        # Create empty dictionaries that will contain information
        # on the municipalities used in this ICM.
        self.cds = {}
        self.mds = {}
        self.vals = {}
        self.icms = {}

    def load_locations(self, changes_file="NL_changes.csv", alpha_mod=1.0,
                       beta_mod=1.0, gamma_mod=1.0, distance_mod=1.0):
        '''
        Combines all locations that were merged over time, and calculates
        all epsilon values for each combination of municipalities.

        @type   regions:    list of lists of strings

        @type   changes_file:   String
        @param  changes_file:   File that contains what municipalities
                                merged into new municipalities. Defaults to
                                "NL_changes.csv".
        @type   alpha_mod:      float
        @param  alpha_mod:      Modifier for the fitted alpha parameter.
                                Defaults to 1.
        @type   beta_mod:       float
        @param  beta_mod:       Modifier for the fitted beta parameter.
                                Defaults to 1.
        @type   gamma_mod:      float
        @param  gamma_mod:      Modifier for the fitted gamma parameter.
                                Defaults to 1.
        @type   delta_mod:      float
        @param  delta_mod:      Modifier for the estimated distance value.
                                Defaults to 1.
        '''

        # Find the mapping for each municipality in the available
        # to the corresponding municipality in the last year specified.
        changes = list(csv.reader(open(self.file_location + "identities/" +
                                       changes_file), delimiter=";"))

        translate = {}
        for change in changes:
            translate[change[0]] = change[1]

        # Retrieve the municipalities known in the last year specified.
        municipalities = MigrationDataSource("NL", self.years[-1]).\
            get_data("inter").keys()

        for municipality in municipalities:
            self.cds[municipality] = {}
            self.mds[municipality] = {}
            translate[municipality] = municipality

        # For each year, combine all the migration data and citizen data
        # available.
        for year in self.years:
            cds = CitizenDataSource("NL", year).get_data()
            mds = MigrationDataSource("NL", year).get_data()

            for municipality in municipalities:
                # Initialise all data for this municipality at zero.
                self.cds[municipality][year] = 0

                self.mds[municipality][year] = {}
                for destination in municipalities:
                    if municipality != destination:
                        self.mds[municipality][year][destination] = 0

                # Find all former and current municipalities that form
                # the current municipality.
                combined = [change[0] for change in changes
                            if change[1] == municipality] + [municipality]

                for mun1 in combined:
                    # Save the citizen data.
                    if mun1 in cds:
                        self.cds[municipality][year] += cds[mun1]['aantal']

                    # Save the intermunicipal migration data.
                    if mun1 in mds:
                        for mun2 in mds[mun1].keys():
                            # If two municipalities are nowadays the same
                            # municipality, disregard the data.
                            if translate[mun1] != translate[mun2]:
                                self.mds[municipality][year][translate[mun2]]\
                                    += int(mds[mun1][mun2])

        # Calculate the migration numbers, corrected for both population
        # and distance.
        for mun1 in self.mds:
            self.vals[mun1] = {}
            total_migrants = {}

            for year in self.years:
                self.vals[mun1][year] = {}
                total_migrants[year] = sum(self.mds[mun1][year].values())

            # Save value and distance in a tuple.
            for mun2 in self.mds:
                if mun1 == mun2:
                    continue

                distance = self.lds.find_distance("NL", mun1, self.years[-1],
                                                  "NL", mun2, self.years[-1])

                for year in self.years:
                    value = (float(self.mds[mun1][year][mun2]) -
                             (0.19839406478 * self.cds[mun1][year] **
                              (0.2433 * alpha_mod) * self.cds[mun2][year] **
                              (0.2327 * beta_mod)) /
                             (distance * distance_mod) **
                             (0.4760 * gamma_mod))

                    self.vals[mun1][year][mun2] = (value, distance)

    def save_locations_file(self):
        '''
        Save the current location data to the file system.
        '''

        with open(self.file_location +
                  'identities/locations.dat', 'wb') as file:
            pickle.dump([self.cds, self.mds, self.vals], file)

    def load_locations_file(self):
        '''
        Load previously saved location data from the file system.
        '''

        with open(self.file_location +
                  'identities/locations.dat', 'rb') as file:
            data = pickle.load(file)
            self.cds = data[0]
            self.mds = data[1]
            self.vals = data[2]

    def calculate_icm(self, regions, changed=None):
        '''
        Calculates the Identity Comparison Measure value for all
        municipalities.

        @type   regions:    list of lists of strings
        @param  regions:    A list of lists of municipalities. Each of these
                            lists represents a region.
        @type   changed:    list of integers
        @param  changed:    Regions that have changed since the last time the
                            ICM value was calculated and should thus be re-
                            calculated.

        @rtype:             (integer, integer)
        @return:            Average ICM value of all municipalities excluding
                            all ICM values that are infinite, and the average
                            ICM value of all municipalities.
        '''

        if changed is None:
            changed = range(0, len(regions))

        for i in changed:
            region = regions[i]

            for municipality in region:
                data = {"in": [], "out": []}

                for year in self.years:
                    data["in"] += [point[1] for point in
                                   self.vals[municipality][year].items()
                                   if point[0] in region and
                                   point[0] != municipality]
                    data["out"] += [point[1] for point in
                                    self.vals[municipality][year].items()
                                    if point[0] not in region and
                                    point[0] != municipality]

                alpha = np.mean([point[0] for point in data["in"]])
                beta = np.mean([point[0] for point in data["out"]])

                self.icms[municipality] = alpha - beta

        return np.mean([val for val in self.icms.values()]), \
            np.median([val for val in self.icms.values()])

    def calculate_icm_file(self, file_name):
        '''
        Calculates the Identity Comparison Measure value for a
        set of regions specified in a file.

        @type   file_name:  String
        @param  file_name:  Name of the file stored in the file location
                            specified in the initialisation of the class.

        @rtype:             (integer, integer)
        @return:            Average ICM value of all municipalities excluding
                            all ICM values that are infinite, and the average
                            ICM value of all municipalities.
        '''

        regions = list(csv.reader(open(str(self.file_location) +
                                       "identities/" + str(file_name) +
                                       '.csv'), delimiter=";"))

        return self.calculate_icm(regions)

    def calculate_icm_random(self, n_regions, grouped=False):
        '''
        Calculates the Identity Comparison Measure value for a
        set of randomly generated regions.

        @type   n_regions:  integer
        @param  n_regions:  The number of randomly generated regions.
        @type   grouped:    boolean
        @param  grouped:    Whether the municipalities in the randomly
                            generated regions should be grouped or not.
                            Defaults to False.

        @rtype:             (integer, integer)
        @return:            Average ICM value of all municipalities excluding
                            all ICM values that are infinite, and the average
                            ICM value of all municipalities.
        '''

        municipalities = list(self.vals.keys())

        # Save the locations of all the municipalities to be able to create
        # clustered groups
        if grouped:
            locations = []

            for municipality in municipalities:
                point = self.lds.find_location("NL", municipality, 2016)
                locations.append([point.x, point.y])

        regions = None

        # Generate random regions, each having a minimum size of 2.
        while regions is None or min([len(region) for region in regions]) < 2:
            regions = [[] for _ in range(n_regions)]

            # If these regions should contain grouped municipalities, use
            # kmeans to generate such clustered regions.
            if grouped:
                kmeans = KMeans(n_clusters=n_regions,
                                random_state=np.random.RandomState()).\
                         fit(locations)

                for i in range(len(municipalities)):
                    regions[kmeans.labels_[i]].append(municipalities[i])
            # If the regions can be fully random, randomly assign
            # municipalities to certain regions.
            else:
                for municipality in municipalities:
                    regions[randint(0, n_regions - 1)].append(municipality)

        return self.calculate_icm(regions)

    def distribution_icm_random(self, n_regions, grouped=False, samples=100,
                                save=False, verbose=True):
        '''
        Creates a distribution of Identity Comparison Measure values of
        sets of randomly generated regions.

        @type   n_regions:  integer
        @param  n_regions:  The number of randomly generated regions.
        @type   grouped:    boolean
        @param  grouped:    Whether the municipalities in the randomly
                            generated regions should be grouped or not.
                            Defaults to False.
        @type   samples:    integer
        @param  samples:    The number of samples taken to create the
                            distribution. Defaults to 100.
        @type   save:       boolean
        @param  save:       If true, save the generated distribution to a file.
                            If false, return the generated distribution.
        @type   verbose:    boolean
        @param  verbose:    If true, output the values found. Can be used
                            to keep track of the progress of the function.
        '''

        values = {"mean": [], "median": []}

        if save:
            file = open(str(self.file_location) +
                        "precalculated/distribution_random" +
                        str(n_regions) + str(grouped) + str(samples) +
                        ".txt", "wb")

        for _ in range(samples):
            mean, median = self.calculate_icm_random(n_regions, grouped)
            values["mean"].append(mean)
            values["median"].append(median)

            if verbose:
                print(mean, median)

            if save:
                pickle.dump(values, file)

        if save:
            file.close()
        else:
            return values

    def distribution_icm_optimized(self, n_regions, file_name=None,
                                   samples=100, save=False, verbose=True,
                                   max_distance=20):
        '''
        Creates a distribution of Identity Comparison Measure values of
        sets of randomly generated regions.

        @type   n_regions:      integer
        @param  n_regions:      The number of used generated regions.
        @type   file_name:      String
        @param  file_name:      Regions specified in this file are used as a
                                starting condition for the optimisation
                                algorithm. Defaults to "None", which means
                                randomly generated clustered regions are
                                optimised.
        @type   grouped:        boolean
        @param  grouped:        Whether the municipalities in the randomly
                                generated regions should be grouped or not.
                                Defaults to False.
        @type   samples:        integer
        @param  samples:        The number of samples taken to create the
                                distribution. Defaults to 100.
        @type   save:           boolean
        @param  save:           If true, save the generated distribution to a
                                file. If false, return the generated
                                distribution.
        @type   verbose:        boolean
        @param  verbose:        If true, output the values found. Can be used
                                to keep track of the progress of the function.
        @type   max_distance:   integer
        @param  max_distance:   Maximum distance at which the nearby identities
                                are considered to be nearby.
        '''

        values = {"mean": [], "median": []}

        if save:
            file = open(str(self.file_location) +
                        "precalculated/distribution_optimized" +
                        str(n_regions) + str(file_name) + str(samples) +
                        "_" + str(max_distance) + ".txt", "wb")

        for _ in range(samples):
            mean, median = self.calculate_icm_optimized(n_regions, file_name,
                                                        max_distance)
            values["mean"].append(mean)
            values["median"].append(median)

            if verbose:
                print(mean, median)

            if save:
                pickle.dump(values, file)

        if save:
            file.close()
        else:
            return values

    def calculate_icm_optimized(self, n_regions, file_name=None,
                                max_distance=20):
        '''
        Optimises a certain set of regions by trying to find the configuration
        that generates the largest ICM value. This can either be a prespecified
        configuration if the file_name is specified, or a certain number of
        randomly generated clustered regions. It can take a long time to
        fully optimise the regions.

        @type   n_regions:  integer
        @param  n_regions:  The number of used generated regions.
        @type   file_name:  String
        @param  file_name:  Regions specified in this file are used as a
                            starting condition for the optimisation algorithm.
                            Defaults to "None", which means randomly generated
                            clustered regions are optimised.
        @type   max_distance:   integer
        @param  max_distance:   Maximum distance at which the nearby identities
                                are considered to be nearby.

        @rtype:             (integer, integer)
        @return:            Average ICM value of all municipalities after the
                            regions are optimised that are not infinite, and
                            the average ICM value of all municipalities after
                            the regions are optimised.
        '''

        # Create a list of all available municipalities.
        municipalities = self.vals.keys()

        # When a file name is specified, use the regions in that file as
        # a starting condition.
        if file_name is not None:
            regions = list(csv.reader(open(str(self.file_location) +
                                           "identities/" + str(file_name) +
                                           ".csv"), delimiter=";"))
        # Otherwise, use random clustered groups as a starting condition.
        else:
            # Save the locations of all the municipalities to be able to create
            # clustered groups
            locations = []

            for municipality in list(municipalities):
                point = self.lds.find_location("NL", municipality, 2016)
                locations.append([point.x, point.y])

            regions = None

            # Generate random regions, each having a minimum size of 2.
            while regions is None or min([len(region)
                                          for region in regions]) < 2:
                regions = [[] for _ in range(n_regions)]

                # If these regions should contain grouped municipalities, use
                # kmeans to generate such clustered regions.
                kmeans = KMeans(n_clusters=n_regions,
                                random_state=np.random.RandomState()).\
                    fit(locations)

                for i in range(len(list(municipalities))):
                    regions[kmeans.labels_[i]].append(list(municipalities)[i])

        # Variables used to keep track of the stopping conditions.
        last_edit = []
        cur_edit = []
        same = 0

        # The optimisation algorithm as described in the paper.

        # Add in a maximum of .. relocation rounds.
        for i in range(0, 50):
            # Used to determine whether anything changed during this
            # iteration.
            changed = False

            # Create new regions, to be filled with the new configuration.
            regions_new = [[] for _ in range(len(regions))]

            # Used to keep track of the number of municipalities an region lost
            # during this iteration.
            lost = [0 for _ in range(len(regions))]

            for mun1 in municipalities:
                choices = []
                cur_region = None

                # Determine the likelihood of the municipality belonging to
                # each particular region.
                for region_id in range(len(regions)):
                    region = regions[region_id]

                    if mun1 in region:
                        cur_region = region_id

                    borders = (cur_region == region_id)

                    # Check whether the region borders the municipality
                    # that is currently checked.
                    for mun2 in region:
                        distance = self.lds.find_distance("NL", mun1,
                                                          self.years[-1],
                                                          "NL", mun2,
                                                          self.years[-1])

                        # If the distance is lower than max_distance kms,
                        # the regions are located close enough to be
                        # considered an option.
                        if distance <= max_distance:
                            borders = True
                            break

                    if not borders:
                        choices.append(0)
                        continue

                    choice_value = 0

                    for mun2 in region:
                        if mun1 == mun2:
                            continue

                        # Calculate the importance of other municipalities
                        # to this municipality
                        choice_value += \
                            np.median([self.vals[mun1][year][mun2][0]
                                       for year in self.years])
                        # Calculate the importance of this municipality
                        # to other municipalities
                        choice_value += \
                            np.median([self.vals[mun2][year][mun1][0]
                                       for year in self.years])

                    # Divide the choice value by the number of other
                    # municipalities used to calculate this choice value.
                    if cur_region == region_id:
                        choices.append(choice_value / (len(region) - 1))
                    else:
                        choices.append(choice_value / len(region))

                if mun1 not in regions[choices.index(max(choices))]:
                    changed = True
                    cur_edit.append(mun1)

                # 50/50 change to move to the better location.
                if randint(0, 1) and len(regions[cur_region]) - \
                   lost[cur_region] > 2:
                    regions_new[choices.index(max(choices))].append(mun1)
                    lost[cur_region] += 1
                else:
                    regions_new[cur_region].append(mun1)

            # Save the new regions as the new region configuration.
            regions, regions_new = regions_new, regions

            if set(last_edit) == set(cur_edit):
                same += 1
            else:
                same = 0

            lost = [0 for _ in range(len(regions))]
            last_edit, cur_edit = cur_edit, []

            # Stop if no municipality should be moved anymore or a status
            # quo is reached (the same municipalities keep changing
            # between different regions for 5 time steps).
            if not changed or same >= 5:
                break

        return self.calculate_icm(regions)

    def different_distributions(self, file_name):
        '''
        Tests for a certain set of regions whether the intraregional
        migration flows differ significantly from the interregional
        migration flows.

        @type   file_name:  String
        @param  file_name:  Regions used to test the differences.

        @rtype:             (integer, integer)
        @return:            Kolmogorov Smirnov test returns
        '''

        regions = list(csv.reader(open(str(self.file_location) +
                                       "identities/" + str(file_name) +
                                       ".csv"), delimiter=";"))

        data = {"in": [], "out": []}

        for i in range(0, len(regions)):
            region = regions[i]

            for municipality in region:
                for year in self.years:
                    data["in"] += [point[1][0] for point in
                                   self.vals[municipality][year].items()
                                   if point[0] in region and
                                   point[0] != municipality]
                    data["out"] += [point[1][0] for point in
                                    self.vals[municipality][year].items()
                                    if point[0] not in region and
                                    point[0] != municipality]

        return ks_2samp(data["in"], data["out"])

    def make_graph(self, data_set=(12, "NL_NUTS2"), data_type="mean",
                   binwidth=0.05):
        '''
        Creates a comparison graph for a certain data set. In this
        comparison graph precalculated distributions of randomly
        generated regions, randomly generated spatially clustered, 
        optimised randomly spatially clustered and optimised regions
        are compared to the ICM value of the original value.

        The figures are saved to the hard drive.

        @type   data_set:   (int, String)
        @param  file_name:  The number of regions and name of the data set.
                            Defaults to (12, "NL_NUTS2").
        @type   data_type:  String
        @param  data_type:  Type of data used in the plot, can either be
                            "mean" ICM value or "median" ICM value. Defaults
                            to "mean".
        @type   bin_width:  float
        @param  bin_width:  Size of the bins used in the graphs.
        '''

        # Load the fully random data.
        objects = []
        with (open(str(self.file_location) + "precalculated/distribution" +
                   "_random" + str(data_set[0]) + "False250.txt",
                   'rb')) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

        full_random = [i for i in objects[-1][data_type]
                       if i > -1000000 and i is not None]

        # Load the randomly spatially clustered data.
        objects = []
        with (open(str(self.file_location) + "precalculated/distribution" +
                   "_random" + str(data_set[0]) + "True250.txt",
                   'rb')) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

        cluster_random = [i for i in objects[-1][data_type]
                          if i > -1000000 and i is not None]

        # Load the optimised data.
        objects = []
        with (open(str(self.file_location) + "precalculated/distribution" +
                   "_optimized" + str(data_set[0]) + str(data_set[1]) +
                   "50_20.txt", 'rb')) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

        optimised = objects[-1][data_type]

        # Load the optimised randomly spatially clustered data.
        objects = []
        with (open(str(self.file_location) + "precalculated/distribution" +
                   "_optimized" + str(data_set[0]) + "None50_20.txt",
                   'rb')) as openfile:
            while True:
                try:
                    objects.append(pickle.load(openfile))
                except EOFError:
                    break

        cluster_optimised = objects[-1][data_type]

        plt.figure(figsize=(10, 5), dpi=110)

        plt.style.use('grayscale')

        true_value = 0

        # Set the titles, true values and limits according to the parameters.
        if data_set[1] == "NL_NUTS2":
            plt.title(data_type.capitalize() + " ICM value distributions " +
                      "for the NUTS 2 regions (provinces)")
            if data_type == "median":
                true_value = 12.347108990177242
                plt.xlim(-2, 15)
            else:
                true_value = 20.90984815361084
                plt.xlim(-2, 25)
        elif data_set[1] == "NL_NUTS3":
            plt.title(data_type.capitalize() + " ICM value distributions " +
                      "for the NUTS 3 regions (COROP regions)")
            if data_type == "median":
                true_value = 33.030871892931586
                plt.xlim(-5, 45)
            else:
                true_value = 59.56796647999738
                plt.xlim(-5, 65)
        elif data_set[1] == "NL_LITERATURE":
            plt.title(data_type.capitalize() + " ICM value distributions " +
                      "for the regions defined by literature")
            if data_type == "median":
                true_value = 42.3382370716107
                plt.xlim(-5, 65)
            else:
                true_value = 73.90728340474173
                plt.xlim(-5, 90)

        # Find the confidence intervals.
        full_random_min = round(sorted(full_random)
                                [int(len(full_random) / 100 * 2.5)], 2)
        full_random_max = round(sorted(full_random)
                                [int(len(full_random) - len(full_random) /
                                     100 * 2.5)], 2)

        cluster_random_min = round(sorted(cluster_random)
                                   [int(len(cluster_random) /
                                        100 * 2.5)], 2)
        cluster_random_max = round(sorted(cluster_random)
                                   [int(len(cluster_random) -
                                        len(cluster_random) /
                                        100 * 2.5)], 2)

        optimised_min = round(sorted(optimised)
                              [int(len(optimised) / 100 * 2.5)], 2)
        optimised_max = round(sorted(optimised)
                              [int(len(optimised) - len(optimised) /
                                   100 * 2.5)], 2)

        cluster_optimised_min = round(sorted(cluster_optimised)
                                      [int(len(cluster_optimised) /
                                           100 * 2.5)], 2)
        cluster_optimised_max = round(sorted(cluster_optimised)
                                      [int(len(cluster_optimised) -
                                           len(cluster_optimised) /
                                           100 * 2.5)], 2)

        plt.hist(full_random,
                 label=r"Randomised regions                  " +
                 "                  " +
                 "($95$% CI [$" + str(full_random_min) + "$, $" +
                 str(full_random_max) + "$])",
                 hatch='//',
                 bins=np.arange(min(full_random), max(full_random) + binwidth,
                                binwidth),
                 edgecolor="none",
                 alpha=0.5,
                 normed=True)

        plt.hist(cluster_random,
                 label=r"Randomised spatially clustered regions    " +
                 "  ($95$% CI [$" + str(cluster_random_min) + "$, $" +
                 str(cluster_random_max) + "$])",
                 hatch='\\',
                 bins=np.arange(min(cluster_random),
                                max(cluster_random) + binwidth, binwidth),
                 edgecolor="none",
                 alpha=0.5,
                 normed=True)

        plt.hist(cluster_optimised,
                 label=r"Optimised spatially clustered regions    " +
                 "     ($95$% CI [$" + str(cluster_optimised_min) + "$, $" +
                 str(cluster_optimised_max) + "$])",
                 hatch='*',
                 bins=np.arange(min(cluster_optimised),
                                max(cluster_optimised) + binwidth, binwidth),
                 edgecolor="none",
                 alpha=0.5,
                 normed=True)

        plt.hist(optimised,
                 label=r"Optimised predefined identity regions" +
                 "        ($95$% CI [$" + str(optimised_min) + "$, $" +
                 str(optimised_max) + "$])",
                 hatch='o',
                 bins=np.arange(min(optimised), max(optimised) + binwidth,
                                binwidth),
                 edgecolor="none",
                 alpha=0.5,
                 normed=True)

        plt.axvline(x=true_value,
                    label=r"Predefined identity regions value            " +
                    "                          ($" +
                    str(round(true_value, 2)) + "$)",
                    linewidth=2)

        plt.axvline(x=max(cluster_optimised),
                    label=r"Maximum optimised spatially clustered regions" +
                    " value      ($" + str(round(max(cluster_optimised), 2)) +
                    "$)",
                    linewidth=2,
                    ls=":")

        plt.axvline(x=max(optimised),
                    label=r"Maximum optimised predefined identity regions" +
                    "  value    ($" + str(round(max(optimised), 2)) + "$)",
                    linewidth=2,
                    ls="dashed")

        font_properties = FontProperties()
        font_properties.set_size('small')
        plt.legend(prop=font_properties, loc="upper center")

        plt.xlabel(data_type.capitalize() + " ICM value of all" +
                   "municipalities in the Netherlands")

        plt.subplots_adjust(left=0.04, right=0.96, top=0.9, bottom=0.1)
        plt.savefig("output/graphs/distributions_" + str(data_type) +
                    str(data_set[1]) + ".png")

    def make_bar_comparison_std_difference(self, data_type="mean"):
        '''
        Creates a bar comparison graph for the different models. To run
        this formula, outcomes should already be saved in a file called
        "configurations.csv"

        The figures are saved to the hard drive.

        @type   data_type:  String
        @param  data_type:  Type of data used in the plot, can either be
                            "mean" ICM value or "median" ICM value. Defaults
                            to "mean".
        '''

        if data_type == "median":
            extend = 6
        else:
            extend = 0

        for (i, dataset, title) in [(0, "NUTS_2", "NUTS 2"),
                                    (1, "NUTS_3", "NUTS 3"),
                                    (2, "LITERATURE", "literature defined")]:
            values = []
            errors = []
            entries = []

            with open(str(self.file_location) +
                      "precalculated/configurations.csv",
                      'r') as csvfile:
                for row in csv.reader(csvfile, delimiter=','):
                    # Skip the first line in the data file.
                    if row[0] == "alpha":
                        continue

                    values.append(float(row[4 + i + extend]))
                    errors.append(float(row[7 + i + extend]))
                    entries.append(r" $\alpha *" + "%.1f" % float(row[0]) +
                                   "," + r"\beta *" + "%.1f" % float(row[1]) +
                                   "," + r"\gamma *" + "%.1f" % float(row[2]) +
                                   "," + r"\Delta *" + "%.1f" % float(row[3]) +
                                   r"$")

            plt.rcdefaults()
            fig, ax = plt.subplots(figsize=(10, 5), dpi=110)
            plt.subplots_adjust(left=0.24, right=0.98, top=0.82, bottom=0.18)
            if data_type == "median":
                plt.xlim(-2, 70)
            else:
                plt.xlim(-2, 14)
            plt.grid(True)

            y_pos = np.arange(len(entries))

            ax.barh(y_pos, values, xerr=errors, align='center',
                    color='gray', ecolor='black')
            ax.set_yticks(y_pos)
            ax.set_yticklabels(entries)
            ax.invert_yaxis()  # labels read top-to-bottom
            ax.set_title('Difference between the ' + data_type + ' ICM value' +
                         ' of\nthe randomly generated spatially clustered' +
                         ' regions\n and ICM value of the predefined ' +
                         title + ' regions')
            ax.set_xlabel('Number of standard deviations of the distribution' +
                          ' of randomly\n generated spatially clustered' +
                          ' regions ' + data_type + ' ICM values')

            plt.savefig("output/graphs/bar_standard_" + str(dataset) +
                        "_" + str(data_type) + ".png")

    def make_bar_comparison_optimisation(self, data_type="mean"):
        '''
        Creates a bar comparison graph for the different models. To run
        this formula, the right distributions should already be generated,
        with a cut-off distance of 10, 20 and 30 kilometres in the optimisation
        algorithm.

        The figures are saved to the hard drive.

        @type   data_type:  String
        @param  data_type:  Type of data used in the plot, can either be
                            "mean" ICM value or "median" ICM value. Defaults
                            to "mean".
        '''

        data = {12: {10: {}, 20: {}, 30: {}}, 40: {10: {}, 20: {}, 30: {}},
                70: {10: {}, 20: {}, 30: {}}}
        objects = []

        files = [[12, "NL_NUTS2", 50, 20],
                 [12, "None", 50, 20],
                 [12, "NL_NUTS2", 30, 10],
                 [12, "None", 30, 10],
                 [12, "NL_NUTS2", 30, 30],
                 [12, "None", 30, 30],
                 [40, "NL_NUTS3", 50, 20],
                 [40, "None", 50, 20],
                 [40, "NL_NUTS3", 30, 10],
                 [40, "None", 30, 10],
                 [40, "NL_NUTS3", 30, 30],
                 [40, "None", 30, 30],
                 [70, "NL_LITERATURE", 50, 20],
                 [70, "None", 50, 20],
                 [70, "NL_LITERATURE", 30, 10],
                 [70, "None", 30, 10],
                 [70, "NL_LITERATURE", 30, 30],
                 [70, "None", 30, 30],
                ]

        for file in files:
            with (open(str(self.file_location) +
                       "precalculated/distribution_optimized" + str(file[0]) +
                       str(file[1]) + str(file[2]) + "_" + str(file[3]) +
                       ".txt", 'rb')) as openfile:
                while True:
                    try:
                        objects.append(pickle.load(openfile))
                    except EOFError:
                        break

            if file[1] == "None":
                index = "random"
            else:
                index = "real"

            data[file[0]][file[3]][index] = objects[-1][data_type]

        entries = []
        values = []
        errors = []

        for size in [(12, "NUTS 2"), (40, "NUTS 3"), (70, "Literature")]:
            for distance in [10, 20, 30]:
                samples = []

                for _ in range(10000):
                    samples.append((np.mean(np.random.choice(data[size[0]]
                                                             [distance]
                                                             ["real"], 20)) -
                                    np.mean(np.random.choice(data[size[0]]
                                                             [distance]
                                                             ["random"], 20))
                                   ) /
                                   np.std(data[size[0]][distance]["random"]))
                values.append(np.mean(samples))
                errors.append(np.std(samples))
                entries.append(str(size[1]) + ", " + str(distance) + " kms")

        plt.rcdefaults()
        fig, ax = plt.subplots(figsize=(10, 5), dpi=110)

        plt.subplots_adjust(left=0.18, right=0.98, top=0.82, bottom=0.18)

        plt.xlim(-8, 8)
        plt.grid(True)

        y_pos = np.arange(len(entries))

        ax.barh(y_pos, values, xerr=errors, align='center',
                color='gray', ecolor='black')
        ax.set_yticks(y_pos)
        ax.set_yticklabels(entries)
        ax.invert_yaxis()  # labels read top-to-bottom
        ax.set_title('Difference between the ' + data_type + ' ICM value of' +
                     '\nthe optimised randomly generated spatially clustered' +
                     'regions\n and ICM value of the optimised predefined' +
                     'regions')
        ax.set_xlabel('Number of standard deviations of the distribution of' +
                      '\n optimised randomly generated spatially clustered' +
                      ' regions ' + data_type + ' ICM values')

        plt.savefig("output/graphs/bar_optimised_" + str(data_type) + ".png")


def main():
    icm = IdentityComparisonMeasure()
    icm.load_locations()

    print(icm.calculate_icm_file("NL_NUTS2"))
    print(icm.calculate_icm_file("NL_NUTS3"))
    print(icm.calculate_icm_file("NL_LITERATURE"))


if __name__ == '__main__':
    main()
