#!/usr/bin/env python3

'''
Gather information from several available data sources.

Written by Willem R.J.Vermeulen.
'''

import pickle
import urllib
import json

from os import listdir
from os.path import isfile, join

import geopandas as gpd
import numpy as np

from shapely.geometry import Point
from .RDWGSConverter import RDWGSConverter


class LocationDataSource(object):
    '''
    Used for determining distances between locations.
    '''

    converter = RDWGSConverter()

    def __init__(self, countries=None, file_location="data/locations/"):
        '''
        Initialise the location data source.

        @type   countries:  List of strings
        @param  countries:  Locations that should be loaded. ["NL"] would
                            load the Netherlands. Defaults to [].
        @type   file_location:  string
        @param  file_location:  Where the location files are stored,
                                defaults to "data/locations/".
        '''

        if countries is None:
            countries = []

        self.file_location = file_location
        self.load(countries)

    def load(self, countries):
        '''
        Load all center points of all municipalities from memory, as well
        as all average distances traveled within municipalities.

        @type   countries:  List of strings
        @param  countries:  Locations that should be loaded. ["NL"] would
                            load the Netherlands. Defaults to [].
        '''

        self.locations = {}
        self.distances = {}

        found = []

        # Detect all datafiles that are currently kept.
        for file in [f for f in listdir(self.file_location)
                     if isfile(join(self.file_location, f))]:
            # Ignore distance files
            if "distances" in file:
                continue

            country = file.split(".")[0]
            found.append(country)

            # If no countries are specified, all information is loaded.
            # Otherwise, only information of countries needed is loaded.
            if countries or country in countries:
                with open(self.file_location + country + '.dat', 'rb') as file:
                    self.locations[country] = pickle.load(file)

                with open(self.file_location + country +
                          '_distances.dat', 'rb') as file:
                    self.distances[country] = pickle.load(file)

        # If a country has no datafile yet, create an empty dictionary.
        for country in countries:
            if country not in found:
                self.locations[country] = {}
                self.distances[country] = {}

    def save(self):
        '''
        Save all center points of all municipalities to memory, as well
        as all average distances traveled within municipalities.
        '''

        for country in self.locations:
            with open(self.file_location + country + '.dat', 'wb') as file:
                pickle.dump(self.locations[country], file)

            with open(self.file_location + country +
                      '_distances.dat', 'wb') as file:
                pickle.dump(self.distances[country], file)

    def save_location(self, country, location, year, point):
        '''
        Check whether a center point for a certain location has already
        been added, and add and save it if the point was not known before.

        @type   country:    string
        @param  country:    Country in which the location is situated:
                            "NL": Netherlands.
        @type   location:   string
        @param  location:   The location for which the center point is saved.
        @type   year:       integer
        @param  year:       The year for which the point was the center of
                            the location.
        @type   point:      tuple of two floats
        @param  point:      x- and y-coordinate for the location.
        '''

        if country not in self.locations:
            self.locations[country] = {}

        if location not in self.locations[country]:
            self.locations[country][location] = {}

        if location in self.locations[country] and \
           year not in self.locations[country][location]:
            self.locations[country][location][year] = point
            self.save()

    def find_distance(self, country1, location1, year1, country2, location2,
                      year2):
        '''
        Calculates the distance between two locations in KM.

        @rtype:             boolean
        @return:            The distance between both locations in KM.
        '''

        if location1 == location2 and country1 == country2 and year1 == year2:
            return self.find_distance_self(country1, location1, year1)

        position1 = self.find_location(country1, location1, year1)
        position2 = self.find_location(country2, location2, year2)

        return position1.distance(position2) / 1000

    def get_points(self, coordinates):
        '''
        Gather a list of all coordinates of points within multiple nested
        arrays of coordinates.

        @type   coordinates:
        @param  coordinates:

        @rtype:             list of tuples of two floats
        @return:            The coordinates of the points.
        '''

        arr = []

        if isinstance(coordinates[0], float):
            arr.append(coordinates)
            return arr

        for coordinate in coordinates:
            arr += self.get_points(coordinate)

        return arr

    def find_location(self, country, location, year):
        '''
        Call the appropiate function based on what country the location is
        located in.

        Currently supports: the Netherlands (1996 - 2017)

        @type   country:    string
        @param  country:    Country in which the location is situated:
                            "NL": Netherlands.
        @type   location:   string
        @param  location:   The name of the location.
        @type   year:       integer
        @param  year:       Year for which the location should be
                            returned.
        '''

        if country == "NL":
            return self.find_location_nl(location, year)

        return None

    def find_distance_self(self, country, location, year):
        '''
        As it is impossible to calculate the distance between a location
        and itself, the average distance by migrants within the location
        is approximated by the average distance between the center of
        the location and its boundaries. This function returns that
        distance.

        @type   country:    string
        @param  country:    Country in which the location is situated:
                            "NL": Netherlands.
        @type   location:   string
        @param  location:   The name of the location.
        @type   year:       integer
        @param  year:       Year for which the distance should be
                            returned.

        @rtype:             float
        @return:            The average distance traveled when moving
                            within the mentioned location.
        '''

        s_location = self.transform_name(location, year)

        if country not in self.distances:
            self.distances[country] = {}

        if s_location not in self.distances[country]:
            self.distances[country][s_location] = {}

        # If this distance has already been calculated before, do not
        # start to calculate again.
        if year in self.distances[country][s_location]:
            return self.distances[country][s_location][year]

        # Gather information on the wherabouts of the location.
        gdf = self.gather_data(s_location, year)

        arr = np.array(self.get_points(gdf['geometry'][0]["coordinates"]))

        length = arr.shape[0]
        sum_y = np.sum(arr[:, 0])
        sum_x = np.sum(arr[:, 1])

        # Determine and save the center coordinates.
        center = Point(self.converter.fromWgsToRd([sum_x / length,
                                                   sum_y / length]))
        self.save_location(country, s_location, year, center)

        # Determine and save the distance.
        distance = 0
        for point in arr:
            point = Point(self.converter.fromWgsToRd([point[1], point[0]]))
            distance += center.distance(point)

        distance /= length * 1000

        if year not in self.distances[country][s_location]:
            self.distances[country][s_location][year] = distance
            self.save()

        return distance

    def find_location_nl(self, location, year):
        '''
        @type   location:   string
        @param  location:   The name of the location.
        @type   year:       integer
        @param  year:       Year for which the location should be
                            returned.
        '''
        s_location = self.transform_name(location, year)

        if s_location not in self.locations["NL"]:
            self.locations["NL"][s_location] = dict()

        if year in self.locations["NL"][s_location]:
            return self.locations["NL"][s_location][year]

        gdf = self.gather_data(s_location, year)

        arr = np.array(self.get_points(gdf['geometry'][0]["coordinates"]))
        length = arr.shape[0]
        sum_y = np.sum(arr[:, 0])
        sum_x = np.sum(arr[:, 1])

        center = Point(self.converter.fromWgsToRd([sum_x / length,
                                                   sum_y / length]))

        self.save_location("NL", s_location, year, center)

        return center

    def transform_name(self, location, year):
        '''
        Returns the name used by http://gemeentegeschiedenis.nl/ to
        identify a certain municipality.

        @type   location:   string
        @param  location:   The name of the location, according to
                            the sources of Statistics Netherlands.
        @type   year:       integer
        @param  year:       Year in which the location existed.
        '''

        s_location = location.replace(" ", "_").replace(",", "_")

        s_location = s_location[0].upper() + s_location[1:]

        # Replace certain commonly used parts of names, and fix
        # three municipalities that cannot be fixed in any other way.
        replaces = [("'", ""), ("_(gemeente)", ""), ("(GLD.)", "Ge"),
                    ("(L.)", "Li"), ("(O.)", "Ov"), ("~~~~", "Ge"),
                    ("~~~~", "NB"), ("(NH.)", "NH"), ("(Z.)", "Ze"),
                    ("(ZH.)", "ZH"), ("Rozendaal", "Rozendaal_Ge"),
                    ("Sudwest-Fryslan", "Sudwest_Fryslan"),
                    ("Valkenisse", "Valkenisse_Walcheren")]

        for pair in replaces:
            s_location = s_location.replace(pair[0], pair[1])

        # Sometimes, multiple municipalities with the same name have existed,
        # which get another ID at gemeentegeschiedenis. (_2)
        splits = [("Abcoude", 1941), ("Albrandswaard", 1985),
                  ("Almelo", 1914), ("Barendrecht", 1905),
                  ("Breukelen", 1949), ("Hardenberg", 1941),
                  ("Heel", 1991), ("Heerenveen", 1934),
                  ("Heerewaarden", 1821), ("Korendijk", 1984),
                  ("Leidschendam", 1938), ("Loenen", 1820),
                  ("Nunspeet", 1972), ("Ommen", 1923), ("Sluis", 2003),
                  ("Waddinxveen", 1870)]

        for pair in splits:
            if pair[0] == location:
                if year >= pair[1]:
                    s_location += "_2"
                else:
                    s_location += "_2"

        return s_location

    def gather_data(self, location, year):
        '''
        Find locations on the gemeentegeschiedenis website.

        @type   location:   string
        @param  location:   ID of the location according to
                            http://gemeentegeschiedenis.nl/
        @type   year:       integer
        @param  year:       Year in which the location existed.

        @rtype:             GeoDataFrame
        @return:            All the available information on the location.
        '''

        try:
            url = "http://www.gemeentegeschiedenis.nl/gemeentenaam/" +\
                  str(location)
            amco = urllib.request.urlopen(url).read().decode('UTF-8')\
                .split('/amco/')[1].split('">')[0]

            url = "http://www.gemeentegeschiedenis.nl/geo/geojson/" +\
                str(amco) + "/" + str(year)

            response = ""

            while response == "":
                response = urllib.request.urlopen(url).read().decode('UTF-8')
        except:
            print("Could not load \"http://www.gemeentegeschiedenis.nl/\" " +
                  "for location \"" + location + "\" in " + str(year))
            return None


        return gpd.GeoDataFrame(json.loads(response)["features"])


def main():
    '''
    Examples of function calls.
    '''

    lds = LocationDataSource(["NL"])
    lds.find_distance("NL", "Diemen", 2015, "NL", "Breda", 2015)


if __name__ == '__main__':
    main()
