#!/usr/bin/env python3

'''
In this file, a GLM is used on Dutch migration data to come up with
a migration model. The GLM can be specified using different sets of
regions, for different years.

Written by Willem R.J.Vermeulen.
'''

import csv

from math import log

import pandas as pd
import statsmodels.api as sm
import statsmodels.formula.api as smf

from .MigrationDataSource import MigrationDataSource
from .CitizenDataSource import CitizenDataSource
from .LocationDataSource import LocationDataSource


def build_migration_model(years=[2016], data_type="all"):
    '''
    Fit a gravity model and extended gravity models using the NUTS 2, NUTS 3
    and literature defined data sets on data for certain years.

    @type   years:      list of integers
    @param  years:      Years of data used to fit the gravity model.
                        Defaults to [2016].
    @type   data_type:  String
                        Used migration data to fit the gravity model. Can
                        either be "all" or "inter", in which case only
                        intermunicipal migration data is used to fit the
                        equations. Defaults to "all".
    '''

    lds = LocationDataSource(["NL"])

    # Dictionary used to store the data for the fitting procedure.
    init_dict = {'pop_origin': [], 'pop_dest': [], 'mig_perc': [], 'distance': [],
                 'same_nuts2': [], 'same_nuts3': [], 'same_literature': [],
                 'intramunicipal': []}

    # All changes in municipalities that have occurred over within the years
    # that the data is from.
    changes = list(csv.reader(open("data/identities/NL_changes.csv"),
                              delimiter=";"))

    # Initiate a look-up table for all municipality names.
    translate = {}
    for change in changes:
        translate[change[0]] = change[1]

    # Load NUTS 2 regions, and update the look-up table.
    areas_nuts2 = list(csv.reader(open("data/identities/NL_NUTS2.csv"),
                                  delimiter=";"))
    mapping_nuts2 = {}

    for i, area in enumerate(areas_nuts2):
        for mun in area:
            mapping_nuts2[mun] = i
            translate[mun] = mun

    # Load NUTS 3 regions, and update the look-up table.
    areas_nuts3 = list(csv.reader(open("data/identities/NL_NUTS3.csv"),
                                  delimiter=";"))
    mapping_nuts3 = {}

    for i, area in enumerate(areas_nuts3):
        for mun in area:
            mapping_nuts3[mun] = i
            translate[mun] = mun

    # Load literature defined regions, and update the look-up table.
    areas_literature = list(csv.reader(open("data/identities/NL_LITERATURE.csv"),
                                       delimiter=";"))
    mapping_literature = {}

    for i, area in enumerate(areas_literature):
        for mun in area:
            mapping_literature[mun] = i
            translate[mun] = mun

    for year in years:
        mds = MigrationDataSource("NL", year).get_data(data_type)
        cds = CitizenDataSource("NL", year).get_data()

        # Gather the migration and population data.
        for origin in mds:
            for destination in mds:
                if origin == destination and data_type != "all":
                    continue

                # log(0) is not possible
                if int(mds[origin][destination]) != 0:
                    init_dict['mig_perc']\
                        .append(log(float(mds[origin][destination]) + 2))
                else:
                    # 2: 74339. 68158.
                    init_dict['mig_perc'].append(log(2))

                distance = lds.find_distance("NL", origin, year, "NL",
                                             destination, year)

                init_dict['pop_origin'].append(log(cds[origin]['aantal']))
                init_dict['pop_dest'].append(log(cds[destination]['aantal']))
                init_dict['distance'].append(log(distance))
                init_dict['same_nuts2']\
                    .append(mapping_nuts2[translate[origin]] ==
                            mapping_nuts2[translate[destination]])
                init_dict['same_nuts3']\
                    .append(mapping_nuts3[translate[origin]] ==
                            mapping_nuts3[translate[destination]])
                init_dict['same_literature']\
                    .append(mapping_literature[translate[origin]] ==
                            mapping_literature[translate[destination]])
                init_dict['intramunicipal'].append(origin == destination)

    print(len(init_dict['pop_origin']))

    # Execute the GLM. No intercept is used, as the number of migrants
    # between two municipalities with 0 residents should be zero.
    formula = "mig_perc ~ pop_origin + pop_dest + distance"

    mod = smf.glm(formula=formula, data=pd.DataFrame(data=init_dict),
                  family=sm.families.Gaussian()).fit()

    # Show the results.
    print(str(mod.summary()))

    # Execute the GLM. No intercept is used, as the number of migrants
    # between two municipalities with 0 residents should be zero.
    formula = "mig_perc ~ pop_origin + pop_dest + distance + C(same_nuts2)"

    mod = smf.glm(formula=formula, data=pd.DataFrame(data=init_dict),
                  family=sm.families.Gaussian()).fit()

    # Show the results.
    print(str(mod.summary()))

    # Execute the GLM. No intercept is used, as the number of migrants
    # between two municipalities with 0 residents should be zero.
    formula = "mig_perc ~ pop_origin + pop_dest + distance + C(same_nuts3)"

    mod = smf.glm(formula=formula, data=pd.DataFrame(data=init_dict),
                  family=sm.families.Gaussian()).fit()

    # Show the results.
    print(str(mod.summary()))

    # Execute the GLM. No intercept is used, as the number of migrants
    # between two municipalities with 0 residents should be zero.
    formula = "mig_perc ~ pop_origin + pop_dest + distance +" +\
              " C(same_literature)"

    mod = smf.glm(formula=formula, data=pd.DataFrame(data=init_dict),
                  family=sm.families.Gaussian()).fit()

    # Show the results.
    print(str(mod.summary()))


if __name__ == '__main__':
    build_migration_model(years=[year for year in range(1996, 2017)],
                         data_type="inter")
