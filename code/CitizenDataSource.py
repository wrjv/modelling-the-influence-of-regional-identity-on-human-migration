#!/usr/bin/env python3

'''
Gather information from the available citizen data sources.

Written by Willem R.J.Vermeulen.
'''

import pandas as pd


class CitizenDataSource(object):
    '''
    Gather information from the specified citizen data source.
    '''

    def __init__(self, country, year=2016, file_location="data/citizens/"):
        '''
        Initialise the citizen data source.

        @type   country:    string
        @param  country:    Country for which the citizen data should be
                            returned: "NL": Netherlands.
        @type   year:       integer
        @param  year:       Year for which the citizen data should be
                            returned.
        '''

        self.country = country
        self.year = year
        self.file_location = file_location

        # Check if the citizen data is available.
        if not self.is_available():
            raise FileNotFoundError("The requested data set does not exist.")

    def is_available(self):
        '''
        Check whether citizen data for a certain country and year is
        available.

        @type   country:    string
        @param  country:    Country for which the citizen data should be
                            returned: "NL": Netherlands.
        @type   year:       integer
        @param  year:       Year for which the citizen data should be
                            returned.

        @rtype:             boolean
        @return:            Whether the data set is available.
        '''

        if self.country == "NL":
            return self.year >= 1988 and self.year <= 2017

        return False

    def get_data(self):
        '''
        Read citizen data from the file system and put it in a dataframe.

        @rtype:             pandas dataframe
        @return:            A table containing all citizen data.
        '''

        data = pd.read_csv(str(self.file_location) + "NL_1988_2017.csv",
                           sep=",", encoding="ISO-8859-1", index_col=1)

        return data.query('jaar=='+str(self.year)).transpose()


def main():
    '''
    Examples of function calls.
    '''

    # Load 2015 citizen data for the Netherlands.
    cds = CitizenDataSource("NL", 2015)

    # Check if the data is available.
    if cds.is_available():
        # Retrieve the citizen data.
        print(cds.get_data())

if __name__ == '__main__':
    main()
