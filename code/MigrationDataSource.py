#!/usr/bin/env python3

'''
Gather information from the available migration data sources.

Written by Willem R.J.Vermeulen.
'''

import pandas as pd


class MigrationDataSource(object):
    '''
    Used to gather migration data for a certain year and country from the file
    system. Either intermunicipal or all migration data (including intra-
    municipal migration data) can be acquired.
    '''

    def __init__(self, country, year, file_location="data/migration/"):
        '''
        Initialise the migration data source.

        @type   country:    string
        @param  country:    Country for which the migration data should be
                            returned: "NL": Netherlands.
        @type   year:       integer
        @param  year:       Year for which the migration data should be
                            returned.
        '''

        self.country = country
        self.year = year

        self.file_location = file_location

        # Check if the migration data is available.
        if not self.is_available():
            raise FileNotFoundError("The requested data set does not exist.")

    def is_available(self):
        '''
        Checks whether migration data for a certain country and year is
        available.

        @type   country:    string
        @param  country:    Country for which the migration data should be
                            returned: "NL": Netherlands.
        @type   year:       integer
        @param  year:       Year for which the migration data should be
                            returned.

        @rtype:             boolean
        @return:            Whether the data set is available.
        '''

        if self.country == "NL":
            return self.year >= 1996 and self.year <= 2016

        return False

    def get_data(self, data_type="all"):
        '''
        Read migration data from the file system and put it in a dataframe.
        Combines intermunicipal and intramunicipal migration data, if needed.

        @type   data_type:  string
        @param  data_type:  Either "all" or "inter", based on what type of
                            migration data should be returned. Usage of the
                            "inter" value excludes migration within
                            municipalities. Defaults to "all".

        @rtype:             pandas dataframe
        @return:            A table containing all migration data, row headers
                            are the migration origin municipalities, column
                            headers the destination municipalities.
        '''

        if data_type not in ["all", "inter"]:
            raise ValueError("The data type \"" + str(data_type) + "\" does" +
                             " not exist.")

        # Read all intermunicipal migration data.
        inter = pd.read_csv(str(self.file_location) + str(self.country) + "/" +
                            str(self.year) + ".csv", sep=",",
                            encoding="ISO-8859-1", index_col=0, header=0)

        if data_type == "inter":
            return inter

        # If all migration data has to be gathered, gather intramunicipal
        # migration data for the current year and append it to the table.
        intra = pd.read_csv(str(self.file_location) + str(self.country) + "/" +
                            "1988_2017_intra.csv", sep=",",
                            encoding="ISO-8859-1", index_col=1, header=0)

        intra = intra.query('jaar=='+str(self.year)).transpose()

        for key in inter.keys():
            inter[key][key] = intra[key]['aantal']

        return inter


def main():
    '''
    Examples of function calls.
    '''

    mds = MigrationDataSource("NL", 2015)
    print(mds.get_data("all"))
    print(mds.get_data("inter"))

if __name__ == '__main__':
    main()
