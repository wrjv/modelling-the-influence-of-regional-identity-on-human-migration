#!/usr/bin/env python3

'''
Class used to create maps of ICM values, to be able to display
geographical distributions.

Written by Willem R. J. Vermeulen
'''

import csv

import fiona
import matplotlib.pyplot as plt

from matplotlib import gridspec
from descartes import PolygonPatch


class MapBuilder(object):
    '''
    Class used to create maps of ICM values, to be able to display
    geographical distributions.
    '''

    def __init__(self, dataset, file_location="data/identities/"):
        '''
        Initiate the map builder. Calculates data to be plotted later
        on, depending on the used parameters.

        @type   dataset:    String
        @param  dataset:    Name of the dataset.
        '''

        # Define the title of the figure, based on the parameters chosen.
        self.title = "Used "

        if dataset == "NL_NUTS2":
            self.title += "NUTS 2 regions (provinces)"
            icm_map = [1, 2, 3, 0, 2, 1, 1, 0, 0, 3, 2, 3]
        elif dataset == "NL_NUTS3":
            self.title += "NUTS 3 regions (COROP regions)"
            icm_map = [2, 1, 3, 2, 1, 0, 1, 0,
                       2, 3, 1, 0, 0, 3, 2, 3,
                       1, 0, 3, 1, 3, 0, 2, 0,
                       1, 2, 0, 0, 3, 2, 2, 0,
                       1, 0, 1, 3, 0, 2, 3, 2]
        elif dataset == "NL_LITERATURE":
            self.title += "historic regions"
            icm_map = [1, 0, 2, 2, 0, 1, 2, 0, 0, 1,
                       1, 2, 0, 0, 1, 3, 2, 3, 3, 3,
                       1, 3, 2, 1, 2, 1, 3, 1, 1, 0,
                       0, 1, 2, 3, 0, 1, 3, 3, 2, 2,
                       3, 0, 3, 1, 3, 2, 0, 0, 2, 1,
                       3, 2, 0, 2, 2, 3, 0, 3, 2, 2,
                       1, 0, 0, 1, 2, 3, 3, 3, 2, 1]

        regions = list(csv.reader(open(file_location +
                                       dataset + ".csv"), delimiter=";"))

        self.icms = {}

        for i, region in enumerate(regions):
            for mun in region:
                self.icms[mun] = icm_map[i]


    def plot(self, name="", file_location="data/borders/"):
        '''
        Save a map of the initialised configuration to a file with a
        prespecified name.

        @type   camp:           String
        @param  cmap:           Type of colours to generate. Defaults to
                                "bw" (grayscale), can also be "rg" (red and)
                                green.
        @type   name:           String
        @param  name:           Filename that should be used for the map.
        @type   file_location:  String
        @param  file_location:  Location of the shape files. Defaults to
                                "data/borders".
        '''

        plt.figure(figsize=(14, 14), dpi=80)
        plt.suptitle(self.title, fontsize=22)
        grid = gridspec.GridSpec(1, 1, width_ratios=[15])
        axs = [plt.subplot(grid[0])]

        plt.axes(axs[0])

        shape = fiona.open(str(file_location) +
                           "Gemeentegrenzen_2016_zonder_water.shp")
        item = shape.next()
        geometries = {}

        translate = {"'s-Gravenhage": "'s-Gravenhage (gemeente)",
                     "Beek": "Beek (L.)",
                     "Bergen (NH)": "Bergen (NH.)",
                     "Bergen (L)": "Bergen (L.)",
                     "Groningen": "Groningen (gemeente)",
                     "Hengelo (O)": "Hengelo (O.)",
                     "Laren": "Laren (NH.)",
                     "Middelburg": "Middelburg (Z.)",
                     "Rijswijk": "Rijswijk (ZH.)",
                     "S9dwest-Frysl1n": "Sudwest-Fryslan",
                     "Stein": "Stein (L.)"}

        try:
            while item:
                if item["properties"]["GM_NAAM"] in translate:
                    mun = translate[item["properties"]["GM_NAAM"]]
                else:
                    mun = item["properties"]["GM_NAAM"]

                geometries[mun] = item["geometry"]
                item = shape.next()
        except:
            pass

        # Plot all different municipalities.
        for mun in geometries:
            geo = geometries[mun]
            val = self.get_colour(mun)
            axs[0].add_patch(PolygonPatch(geo, fc=val, ec=(0, 0, 0),
                                          alpha=0.8, zorder=2))

        axs[0].axis('scaled')
        plt.axis('off')

        plt.subplots_adjust(left=0, right=0.9, top=0.9, bottom=0.02)

        plt.savefig(name, dpi=600)

    def get_colour(self, municipality=""):
        '''
        Translate colours to a certain colour map based on certain maximum
        and minimum values. These colours can be used to make map plots.

        @type   municipality    String
        @param  municipality    Name of the municipality as used in
                                self.icms. Defaults to empty, in which case
                                the "value" parameter is used.
        @type   value           float
        @param  value           Value to be mapped to a certain colour.

        @rtype                  (float, float, float)
        @returns                (red, green, blue) on a 0-1 scale.
        '''

        colors = [(22 / 255.0, 58 / 255.0, 114 / 255.0), # Blue
                  (99 / 255.0, 186 / 255.0, 89 / 255.0), # Green
                  (255 / 255.0, 249 / 255.0, 150 / 255.0), # Yellow
                  (150 / 255.0, 43 / 255.0, 43 / 255.0)] # Red

        if municipality != "":
            return colors[self.icms[municipality]]

        return (0, 0, 0)


def main():
    ''' Create the three sample graphs. '''

    mapbuilder = MapBuilder("NL_LITERATURE")
    mapbuilder.plot(name="used_historic.png")
    mapbuilder = MapBuilder("NL_NUTS2")
    mapbuilder.plot(name="used_nuts2.png")
    mapbuilder = MapBuilder("NL_NUTS3")
    mapbuilder.plot(name="used_nuts3.png")

if __name__ == '__main__':
    main()
