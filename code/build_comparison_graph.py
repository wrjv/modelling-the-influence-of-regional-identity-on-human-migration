#!/usr/bin/env python3

'''
Create a graph to compare the percentual difference in the predicted number
of migrants for the different gravity models.

Written by Willem R.J.Vermeulen.
'''

import numpy as np
import matplotlib.pyplot as plt

def gravity_basic(population1, population2, distance):
    '''
    Formula for the basic gravity model.
    '''

    return np.exp(-1.6175) * population1 ** 0.2433 * population2 ** 0.2327 /\
        distance ** 0.4760

def gravity_nuts2(population1, population2, distance, same_region=False):
    '''
    Formula for the basic gravity model extended with NUTS 2 regions.
    '''

    basic = np.exp(-2.0427) * population1 ** 0.2452 * population2 ** 0.2343 /\
        distance ** 0.3987

    if same_region:
        return basic * np.exp(0.3824)

    return basic

def gravity_nuts3(population1, population2, distance, same_region=False):
    '''
    Formula for the basic gravity model extended with NUTS 3 regions.
    '''

    basic = np.exp(-2.3727) * population1 ** 0.2491 * population2 ** 0.2361 /\
        distance ** 0.3373

    if same_region:
        return basic * np.exp(1.2482)

    return basic

def gravity_historic(population1, population2, distance, same_region=False):
    '''
    Formula for the basic gravity model extended with historic regions.
    '''

    basic = np.exp(-2.3398) * population1 ** 0.2499 * population2 ** 0.2383 /\
        distance ** 0.3493

    if same_region:
        return basic * np.exp(1.5360)

    return basic


def build_comparison_graph(name):
    '''
    Create a graph of the difference between the predicted number of migrants
    in the standard and extended gravity models.
    '''

    distances = np.arange(0.5, 250, 0.001)
    distances_id1 = np.arange(0.5, 40, 0.001)
    distances_id2 = np.arange(0.5, 25, 0.001)
    distances_id3 = np.arange(0.5, 20, 0.001)

    pop1, pop2 = 10000, 10000

    base = gravity_basic(pop1, pop2, distances)
    base_id1 = gravity_basic(pop1, pop2, distances_id1)
    base_id2 = gravity_basic(pop1, pop2, distances_id2)
    base_id3 = gravity_basic(pop1, pop2, distances_id3)

    colors = [(22 / 255.0, 58 / 255.0, 114 / 255.0), # Blue
              (99 / 255.0, 186 / 255.0, 89 / 255.0), # Green
              (255 / 255.0, 249 / 255.0, 150 / 255.0), # Yellow
              (150 / 255.0, 43 / 255.0, 43 / 255.0)] # Red

    plt.plot(distances, 100 * (gravity_nuts2(pop1, pop2, distances) - base)
             / base, color=colors[0], label="NUTS 2")
    plt.plot(distances_id1, 100 * (gravity_nuts2(pop1, pop2, distances_id1, True) - base_id1)
             / base_id1, linestyle="--", color=colors[0], label="NUTS 2 (same region)")
    plt.plot(distances, 100 * (gravity_nuts3(pop1, pop2, distances) - base)
             / base, color=colors[1], label="NUTS 3")
    plt.plot(distances_id2, 100 * (gravity_nuts3(pop1, pop2, distances_id2, True) - base_id2)
             / base_id2, linestyle="--", color=colors[1], label="NUTS 3 (same region)")
    plt.plot(distances, 100 * (gravity_historic(pop1, pop2, distances) - base)
             / base, color=colors[3], label="Historic")
    plt.plot(distances_id3, 100 * (gravity_historic(pop1, pop2, distances_id3, True) - base_id3)
             / base_id3, linestyle="--", color=colors[3], label="Historic (same region)")

    plt.legend()
    plt.xscale('log')
    plt.xlabel("Distance traveled between two municipalities (KM)")
    plt.ylabel("Change in the predicted number of migrants (%)")
    plt.title("Difference between the predicted number of migrants \n" +
              " in the standard and extended gravity models")
    plt.savefig(name, dpi=600)

if __name__ == '__main__':
    create_plot()
