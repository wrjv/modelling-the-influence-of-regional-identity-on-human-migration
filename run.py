#!/usr/bin/env python3

'''
Code used to make all function calls and gather all output.

Written by Willem R. J. Vermeulen.
'''

import numpy as np

from code.MapBuilder import MapBuilder
from code.IdentityComparisonMeasure import IdentityComparisonMeasure
from code.build_model import build_migration_model
from code.build_comparison_graph import build_comparison_graph

def create_maps():
    '''
    Create maps with the different areas, used in the supplementary material.
    '''

    print("Creating maps...")
    mapbuilder = MapBuilder("NL_LITERATURE")
    mapbuilder.plot(name="output/maps/used_historic.pdf")
    mapbuilder = MapBuilder("NL_NUTS2")
    mapbuilder.plot(name="output/maps/used_nuts2.pdf")
    mapbuilder = MapBuilder("NL_NUTS3")
    mapbuilder.plot(name="output/maps/used_nuts3.pdf")


def find_icm_values(load_from_file=False):
    '''
    Makes all basic calls that use the ICM.

    @type   generate:       Boolean
    @param  generate:       Whether the distributions should be loaded or
                            generated. Defaults to False.
    '''

    print("Loading IdentityComparisonMeasure...")
    icm = IdentityComparisonMeasure()
    print("  Loading locations...")

    if load_from_file:
        icm.load_locations_file()
    else:
        icm.load_locations()
        icm.save_locations_file()

    print("  Finding ICM values...")
    print(icm.calculate_icm_file("NL_NUTS2"))
    print(icm.calculate_icm_file("NL_NUTS3"))
    print(icm.calculate_icm_file("NL_LITERATURE"))

    print("  Testing distributions...")
    result = icm.different_distributions("NL_NUTS2")
    print("    NUTS 2:     [KS Statistic: " + str(result[0]) +
          ", p-value: " + str(result[1]) + "]")
    result = icm.different_distributions("NL_NUTS3")
    print("    NUTS 3:     [KS Statistic: " + str(result[0]) +
          ",  p-value: " + str(result[1]) + "]")
    result = icm.different_distributions("NL_LITERATURE")
    print("    Literature: [KS Statistic: " + str(result[0]) +
          ",  p-value: " + str(result[1]) + "]")


    print("  Finding ICM values of randomly generated regions...")
    print(icm.distribution_icm_random(12, grouped=False, samples=250))
    print(icm.distribution_icm_random(12, grouped=True, samples=250))
    print(icm.distribution_icm_optimized(12, "NL_NUTS2", 50))
    print(icm.distribution_icm_optimized(12, None, 50))

    print(icm.distribution_icm_random(40, grouped=False, samples=250))
    print(icm.distribution_icm_random(40, grouped=True, samples=250))
    print(icm.distribution_icm_optimized(40, "NL_NUTS3", 50))
    print(icm.distribution_icm_optimized(40, None, 50))

    print(icm.distribution_icm_random(70, grouped=False, samples=250))
    print(icm.distribution_icm_random(70, grouped=True, samples=250))
    print(icm.distribution_icm_optimized(70, "NL_LITERATURE", 50))
    print(icm.distribution_icm_optimized(70, None, 50))

    print(icm.distribution_icm_optimized(12, None, 30, max_distance=10))
    print(icm.distribution_icm_optimized(12, "NL_NUTS2", 30, max_distance=10))
    print(icm.distribution_icm_optimized(12, None, 30, max_distance=30))
    print(icm.distribution_icm_optimized(12, "NL_NUTS2", 30, max_distance=30))

    print(icm.distribution_icm_optimized(40, None, 30, max_distance=10))
    print(icm.distribution_icm_optimized(40, "NL_NUTS3", 30, max_distance=10))
    print(icm.distribution_icm_optimized(40, None, 30, max_distance=30))
    print(icm.distribution_icm_optimized(40, "NL_NUTS3", 30, max_distance=30))

    print(icm.distribution_icm_optimized(70, None, 30, max_distance=10))
    print(icm.distribution_icm_optimized(70, "NL_LITERATURE", 30,
                                         max_distance=10))
    print(icm.distribution_icm_optimized(70, None, 30, max_distance=30))
    print(icm.distribution_icm_optimized(70, "NL_LITERATURE", 30,
                                         max_distance=30))

def parameter_sensitivity(samples=50):
    '''
    Test the sensitivity of the model to changing certain parameters.

    @type   samples:    integer
    @param  samples:    Number of samples used to test the parameter
                        sensitivity.
    '''

    configs = [(1.0, 1.0, 1.0, 1.0),
               (0.9, 0.9, 0.9, 0.9),
               (1.1, 1.1, 1.1, 1.1),
               (0.9, 1.0, 1.0, 1.0),
               (1.0, 0.9, 1.0, 1.0),
               (1.0, 1.0, 0.9, 1.0),
               (1.0, 1.0, 1.0, 0.9),
               (1.1, 1.0, 1.0, 1.0),
               (1.0, 1.1, 1.0, 1.0),
               (1.0, 1.0, 1.1, 1.0),
               (1.0, 1.0, 1.0, 1.1)]

    for (alpha_mod, beta_mod, gamma_mod, distance_mod) in configs:
        print("  Loading IdentityComparisonMeasure...")
        icm = IdentityComparisonMeasure()
        print("    Loading Locations... (alpha * " + str(alpha_mod) +
              ", beta * " + str(beta_mod) + ", gamma * " + str(gamma_mod) +
              ", distance * " + str(distance_mod) + ")")
        icm.load_locations(alpha_mod=alpha_mod, beta_mod=beta_mod,
                           gamma_mod=gamma_mod, distance_mod=distance_mod)
        print("    Finding ICM values...")

        distribution = icm.distribution_icm_random(12, grouped=True,
                                                   samples=samples, save=False,
                                                   verbose=False)
        true_value = icm.calculate_icm_file("NL_NUTS2")
        samples_mean = []
        samples_median = []

        # Generate samples to find a good standard deviation.
        for _ in range(10000):
            dataset = np.random.choice(distribution["mean"], 20)
            samples_mean.append((true_value[0] - np.mean(dataset)) /
                                np.std(dataset))
            dataset = np.random.choice(distribution["median"], 20)
            samples_median.append((true_value[1] - np.median(dataset)) /
                                  np.std(dataset))

        print("      NUTS 2 std difference (mean):         " +
              str(np.mean(samples_mean)) + " , err: " +
              str(np.std(samples_mean)))
        print("      NUTS 2 std difference (median):       " +
              str(np.mean(samples_median)) + " , err: " +
              str(np.std(samples_median)))

        distribution = icm.distribution_icm_random(40, grouped=True,
                                                   samples=samples, save=False,
                                                   verbose=False)
        true_value = icm.calculate_icm_file("NL_NUTS3")
        samples_mean = []
        samples_median = []

        # Generate samples to find a good standard deviation.
        for _ in range(10000):
            dataset = np.random.choice(distribution["mean"], 20)
            samples_mean.append((true_value[0] - np.mean(dataset)) /
                                np.std(dataset))
            dataset = np.random.choice(distribution["median"], 20)
            samples_median.append((true_value[0] - np.median(dataset)) /
                                  np.std(dataset))

        print("      NUTS 3 std difference (mean):         " +
              str(np.mean(samples_mean)) + " , err: " +
              str(np.std(samples_mean)))
        print("      NUTS 3 std difference (median):       " +
              str(np.mean(samples_median)) + " , err: " +
              str(np.std(samples_median)))

        distribution = icm.distribution_icm_random(70, grouped=True,
                                                   samples=samples, save=False,
                                                   verbose=False)
        true_value = icm.calculate_icm_file("NL_LITERATURE")
        samples_mean = []
        samples_median = []

        # Generate samples to find a good standard deviation.
        for _ in range(10000):
            dataset = np.random.choice(distribution["mean"], 20)
            samples_mean.append((true_value[0] - np.mean(dataset)) /
                                np.std(dataset))
            dataset = np.random.choice(distribution["median"], 20)
            samples_median.append((true_value[0] - np.median(dataset)) /
                                  np.std(dataset))

        print("      Literature std difference (mean):     " +
              str(np.mean(samples_mean)) + " , err: " +
              str(np.std(samples_mean)))
        print("      Literature std difference (median):   " +
              str(np.mean(samples_median)) + " , err: " +
              str(np.std(samples_median)))

        print("    Testing distributions...")
        result = icm.different_distributions("NL_NUTS2")
        print("      NUTS 2:     [KS Statistic: " +
              str(result[0]) + ",\t p-value: " + str(result[1]) + "]")
        result = icm.different_distributions("NL_NUTS3")
        print("      NUTS 3:     [KS Statistic: " +
              str(result[0]) + ",\t p-value: " + str(result[1]) + "]")
        result = icm.different_distributions("NL_LITERATURE")
        print("      Literature: [KS Statistic: " +
              str(result[0]) + ",\t p-value: " + str(result[1]) + "]")


def main():
    # build_migration_model(years=[year for year in range(1996, 2017)],
    #                       data_type="all")
    # build_comparison_graph(name="output/graphs/comparison_graph.pdf")
    # create_maps()
    # find_icm_values()
    # parameter_sensitivity(samples=50)
    pass

if __name__ == '__main__':
    main()
